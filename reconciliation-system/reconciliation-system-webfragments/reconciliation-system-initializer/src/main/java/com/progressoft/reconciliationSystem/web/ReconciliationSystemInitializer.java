package com.progressoft.reconciliationSystem.web;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

public class ReconciliationSystemInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        SourceUpload sourceUpload = new SourceUpload();
        TargetUpload targetUpload = new TargetUpload();
        LoginServlet login = new LoginServlet();
        ReadyToCompare readyToCompare = new ReadyToCompare();
        ResultView result = new ResultView();


        ServletRegistration.Dynamic targetUploadRegistration = ctx.addServlet("targetUpload", targetUpload);
        targetUploadRegistration.addMapping("/targetUpload");

        ServletRegistration.Dynamic sourceUploadRegistration = ctx.addServlet("sourceUpload", sourceUpload);
        sourceUploadRegistration.addMapping("/sourceUpload");

        ServletRegistration.Dynamic loginRegistration = ctx.addServlet("login", login);
        loginRegistration.addMapping("/login");

        ServletRegistration.Dynamic readyToCompareRegistration = ctx.addServlet("readyToCompare", readyToCompare);
        readyToCompareRegistration.addMapping("/readyToCompare");

        ServletRegistration.Dynamic resultRegistration = ctx.addServlet("result", result);
        resultRegistration.addMapping("/result");

    }
}
