package com.progressoft.reconciliationSystem.web;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServletTest {
    @Test
    public void givenValidPostRequest_whenDoPost_thenRedirectToSourceUpload() throws IOException, ServletException {
        LoginServlet login = new LoginServlet();

        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        Mockito.when(request.getMethod()).thenReturn("POST");

        login.service(request, response);

        Mockito.verify(response).sendRedirect("/sourceUpload");
    }
}
