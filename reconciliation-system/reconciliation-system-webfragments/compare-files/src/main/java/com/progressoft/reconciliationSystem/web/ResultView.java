package com.progressoft.reconciliationSystem.web;

import com.progressoft.reconciliationSystem.managment.FileDetails;
import com.progressoft.reconciliationSystem.managment.WebReconciliationGenerator;
import com.progressoft.reconciliationSystem.matching.RecordMatcher;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ResultView extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        try {
            showResultAndGetPath(req);

            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/views/result.jsp");
            requestDispatcher.forward(req, response);
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE,e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws IOException {
        try {
            Path tempDirectoryPath = showResultAndGetPath(req);
            download(response, tempDirectoryPath);
        }catch (Exception e){
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE,e.getMessage());
        }
    }

    private void download(HttpServletResponse response, Path tempDirectoryPath) throws IOException {
        File directory = tempDirectoryPath.toFile();
        String[] filesList = directory.list();
        if (filesList != null && filesList.length > 0) {
            ServletOutputStream sos = response.getOutputStream();
            // TODO pass output stream to the zipFiles method
            byte[] zip = zipFiles(directory, filesList);
            response.setContentType("application/zip");
            response.setHeader("Content-Disposition", "attachment; filename=Result.ZIP");
            sos.write(zip);
            sos.flush();
        }
    }

    private Path showResultAndGetPath(HttpServletRequest req) {
        String resultFileFormat = getFromSession(req.getSession(),"FileFormat");
        FileDetails sourceFile = getSourceFileDetails(req);
        FileDetails targetFile = getTargetFileDetails(req);

        WebReconciliationGenerator generator = new WebReconciliationGenerator(resultFileFormat, req, new RecordMatcher());
        return generator.generate(sourceFile, targetFile);
    }

    private FileDetails getTargetFileDetails(HttpServletRequest req) {
        String targetPath = getFromSession(req.getSession(),"TargetPath");
        String targetType = getFromSession(req.getSession(),"TargetType");
        String targetOriginalFileName = getFromSession(req.getSession(),"TargetOriginalFileName");

        return new FileDetails(targetPath + "/" + targetOriginalFileName, targetType);
    }

    private FileDetails getSourceFileDetails(HttpServletRequest req) {
        String sourcePath = getFromSession(req.getSession(),"SourcePath");
        String sourceType = getFromSession(req.getSession(),"SourceType");
        String sourceOriginalFileName = getFromSession(req.getSession(), "SourceOriginalFileName");
        return new FileDetails(sourcePath + "/" + sourceOriginalFileName, sourceType);
    }

    private String getFromSession(HttpSession session,String value){
        return (String) session.getAttribute(value);
    }


    // TODO you could write to the response outputstream directly
    private byte[] zipFiles(File directory, String[] files) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(baos);
        byte bytes[] = new byte[2048];
        for (String fileName : files) {
            FileInputStream fis = new FileInputStream(directory.getPath() +
                    "/" + fileName);
            BufferedInputStream bis = new BufferedInputStream(fis);
            zos.putNextEntry(new ZipEntry(fileName));
            int bytesRead;
            while ((bytesRead = bis.read(bytes)) != -1) {
                zos.write(bytes, 0, bytesRead);
            }
            zos.closeEntry();
            bis.close();
            fis.close();
        }
        zos.flush();
        baos.flush();
        zos.close();
        baos.close();
        return baos.toByteArray();
    }

}
