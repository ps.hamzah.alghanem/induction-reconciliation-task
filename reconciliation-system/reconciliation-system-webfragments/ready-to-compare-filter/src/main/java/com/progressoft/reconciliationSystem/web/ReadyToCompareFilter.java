package com.progressoft.reconciliationSystem.web;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ReadyToCompareFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest= (HttpServletRequest) request;
        HttpServletResponse httpResp = (HttpServletResponse) response;
        String sourceFileName = (String) httpRequest.getSession().getAttribute("SourceFileName");
        String targetFileName= (String) httpRequest.getSession().getAttribute("TargetFileName");
        if(sourceFileName==null){
            httpResp.sendRedirect("/sourceUpload");
            return;
        }
        if(targetFileName==null){
            httpResp.sendRedirect("/targetUpload");
            return;
        }
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
