package com.progressoft.reconciliationSystem.web;

import com.progressoft.reconciliationSystem.LoginCheckerFilter;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginCheckerFilterTest {
    @Test
    public void givenInvalidSession_whenDoFilter_thenRedirectToLogin() throws IOException, ServletException {
        LoginCheckerFilter filter=new LoginCheckerFilter();

        HttpServletRequest request= Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response=Mockito.mock(HttpServletResponse.class);
        FilterChain chain=Mockito.mock(FilterChain.class);
        HttpSession session=Mockito.mock(HttpSession.class);

        Mockito.when(request.getSession()).thenReturn(session);
        Mockito.when(request.getRequestURI()).thenReturn("/sourceUpload");


        filter.doFilter(request,response,chain);

        Mockito.verify(response).sendRedirect("/");
    }
}
