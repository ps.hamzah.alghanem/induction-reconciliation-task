package com.progressoft.reconciliationSystem;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;

public class LoginCheckerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest= (HttpServletRequest) request;
        String username = httpRequest.getParameter("username");
        String password = httpRequest.getParameter("password");
        String method = httpRequest.getMethod();

        if(username==null || password==null || method.equalsIgnoreCase("get")){
            sendRedirectToLogin((HttpServletResponse) response);
            return;
        }
        if(!(username.equals("admin") && password.equals("admin"))){
            sendRedirectToLogin((HttpServletResponse) response);
            return;
        }
        httpRequest.getSession().setAttribute("username",username);
        httpRequest.getSession().setAttribute("password",password);
        chain.doFilter(httpRequest,response);
    }

    private void sendRedirectToLogin(HttpServletResponse response) throws IOException {
        response.sendRedirect("/");
    }

    @Override
    public void destroy() {

    }
}
