package com.progressoft.reconciliationSystem.web;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HomeAccessControl implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest= (HttpServletRequest) request;
        String username = (String) httpRequest.getSession().getAttribute("username");
        String password = (String) httpRequest.getSession().getAttribute("password");
        if(username!=null || password!=null){
            sendRedirectToSourceUpload((HttpServletResponse) response);
            return;
        }
        chain.doFilter(httpRequest,response);
    }

    private void sendRedirectToSourceUpload(HttpServletResponse response) throws IOException {
        response.sendRedirect("/sourceUpload");
    }

    @Override
    public void destroy() {

    }
}
