package com.progressoft.reconciliationSystem.web;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

// TODO enhance: this could be enhanced by accepting request and response as parameters to uploadFile
// then you can have this class injected as dependency to the servlets (stateless and stateful)
public class FileLoader {
    private HttpServletRequest request;
    private HttpServletResponse response;

    public FileLoader(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public void uploadFile(String fileName) throws IOException {
        String tempPath = String.valueOf(Files.createTempDirectory("temp"));
        request.getSession().setAttribute(fileName + "Path", tempPath);
        try {
            // TODO enhance use request.getPart
            List<FileItem> multiParts = new ServletFileUpload(
                    new DiskFileItemFactory()).parseRequest(request);
            FileItem file = null;
            String newFileName = null, type = null, originalFileName = null;
            for (FileItem item : multiParts) {
                if (!item.isFormField()) {
                    originalFileName = item.getName();
                    file = item;
                } else {
                    if (item.getFieldName().equalsIgnoreCase(fileName)) {
                        newFileName = item.getString();
                    } else if (item.getFieldName().equalsIgnoreCase("types")) {
                        type = item.getString();
                    }
                }
            }
            request.getSession().setAttribute(fileName, newFileName);
            request.getSession().setAttribute(fileName + "Type", type);
            request.getSession().setAttribute(fileName + "FileName", newFileName);
            request.getSession().setAttribute(fileName + "OriginalFileName", originalFileName);
            file.write(new File(tempPath + File.separator + originalFileName));
        } catch (Exception ex) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, String.valueOf(ex.getCause()));
            // TODO this send error is not communicated with the calling servlet or throw an exception and let the caller
            // decide what to do
        }
    }
}
