package com.progressoft.reconciliationSystem.web;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticatorFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest= (HttpServletRequest) request;
        String username = (String) httpRequest.getSession().getAttribute("username");
        String password = (String) httpRequest.getSession().getAttribute("password");
        if(username==null || password==null){
            sendRedirectToLogin((HttpServletResponse) response);
        }
        if(!(username.equals("admin") && password.equals("admin"))){
            sendRedirectToLogin((HttpServletResponse)response);
            return;
        }
        chain.doFilter(httpRequest,response);
    }

    @Override
    public void destroy() {

    }

    private void sendRedirectToLogin(HttpServletResponse response) throws IOException {
        response.sendRedirect("/");
    }
}
