package com.progressoft.reconciliationSystem.readFiles;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class FileReaderTest {

    Path path;

    @Test
    public void givenInvalidPathOrInvalidExtension_whenConstructing_thenThrowException() {
        path = null;
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> new FileReader(path, "csv"));
        Assertions.assertEquals("null path", exception.getMessage());
        path = Paths.get("/home/user/fx.csv");
        Assertions.assertThrows(ReadException.class, () -> new FileReader(path, "csv"));
        path = Paths.get("src/test/resources/bank-transactions.csv");
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> new FileReader(path, null));
        Assertions.assertEquals("null extension", illegalArgumentException.getMessage());
        path = Paths.get("src/test/resources/unusefulFile.csv");
        illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> new FileReader(path, "json"));
        Assertions.assertEquals("file extension mismatch the expected extension", illegalArgumentException.getMessage());
        path = Paths.get("src/test/resources/unusefulFile.pdf");
        illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> new FileReader(path, "pdf"));
        Assertions.assertEquals("file extension doesn't provided", illegalArgumentException.getMessage());
    }

    @Test
    public void givenUnusefulFile_whenReading_thenThrowIllegalStateException() throws FileNotFoundException {
        Path path = Paths.get("src/test/resources/unusefulFile.csv");
        FileReader fileReader = null;
        fileReader = new FileReader(path, "csv");
        Assertions.assertThrows(IllegalStateException.class, fileReader::readNextRow);
    }

    @Test
    public void givenValidFile_whenReading_thenReturnNullWhenFinish(){
        path=Paths.get("src/test/resources/online-banking-transactions.json");
        FileReader fileReader=new FileReader(path,"json");
        Record line;
        while((line=fileReader.readNextRow())!=null){
        }
        Assertions.assertTrue(Objects.isNull(line));
    }
}
