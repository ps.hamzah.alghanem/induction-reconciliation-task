package com.progressoft.reconciliationSystem.readFiles;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class JSONReaderTest {
    Path path;

    @Test
    public void givenNullPath_whenConstructing_thenThrowIllegalStateException() {
        path = null;
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> new JSONReader(path));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenInvalidPath_whenConstructing_thenThrowIllegalStateException() {
        path = Paths.get("home/user");
        ReadException exception = Assertions.assertThrows(ReadException.class, () -> new JSONReader(path));
        Assertions.assertEquals("file doesn't exist", exception.getMessage());
    }

    @Test
    public void givenInvalidDateFormatInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("src/test/resources/online-banking-transactions1.json");
        JSONReader jsonReader = new JSONReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, jsonReader::read);
        Assertions.assertEquals("Error at index : 1", illegalStateException.getMessage());
    }

    @Test
    public void givenNegativeAmountInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("src/test/resources/online-banking-transactions2.json");
        JSONReader jsonReader = new JSONReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, jsonReader::read);
        Assertions.assertEquals("Amount have negative value at index : 1", illegalStateException.getMessage());
    }

    @Test
    public void givenInvalidMoneyFormatInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("src/test/resources/online-banking-transactions3.json");
        JSONReader jsonReader = new JSONReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, jsonReader::read);
        Assertions.assertEquals("Error at index : 1", illegalStateException.getMessage());
    }

    @Test
    public void givenValidFile_whenReading_thenReturnNullWhenFinish() {
        path = Paths.get("src/test/resources/online-banking-transactions.json");
        JSONReader jsonReader = new JSONReader(path);
        Record line;
        while ((line = jsonReader.read()) != null) {
        }
        Assertions.assertTrue(Objects.isNull(line));
    }
}
