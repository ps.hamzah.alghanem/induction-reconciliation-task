package com.progressoft.reconciliationSystem.readFiles;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class CSVReaderTest {
    Path path;

    @Test
    public void givenNullPath_whenConstructing_thenThrowIllegalStateException() {
        path = null;
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> new CSVReader(path));
        Assertions.assertEquals("null path", exception.getMessage());
    }

    @Test
    public void givenInvalidPath_whenConstructing_thenThrowIllegalStateException() {
        path = Paths.get("home/user");
        ReadException exception = Assertions.assertThrows(ReadException.class, () -> new CSVReader(path));
        Assertions.assertEquals("file doesn't exist", exception.getMessage());
    }

    @Test
    public void givenInvalidDateFormatInsideCSVFile_whenReading_thenThrowIllegalStateException() throws FileNotFoundException {
        path = Paths.get("src/test/resources/bank-transactions1.csv");
        CSVReader csvReader = new CSVReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::read);
        Assertions.assertEquals("Wrong date format at index : 1", illegalStateException.getMessage());
    }

    @Test
    public void givenNegativeAmountInsideCSVFile_whenReading_thenThrowIllegalStateException() throws FileNotFoundException {
        path = Paths.get("src/test/resources/bank-transactions2.csv");
        CSVReader csvReader = new CSVReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::read);
        Assertions.assertEquals("Amount have negative value at index : 1", illegalStateException.getMessage());
    }

    @Test
    public void givenInvalidMoneyFormatInsideFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("src/test/resources/bank-transactions3.csv");
        CSVReader csvReader = new CSVReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::read);
        Assertions.assertEquals("Amount have wrong format at index : 1 ", illegalStateException.getMessage());
    }

    @Test
    public void givenInconsistencyFile_whenReading_thenThrowIllegalStateException() {
        path = Paths.get("src/test/resources/bank-transactions4.csv");
        CSVReader csvReader = new CSVReader(path);
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class, csvReader::read);
        Assertions.assertEquals("Inconsistency file", illegalStateException.getMessage());
    }

    @Test
    public void givenValidFile_whenReading_thenReturnNullWhenFinish(){
        path=Paths.get("src/test/resources/bank-transactions.csv");
        CSVReader csvReader=new CSVReader(path);
        Record line;
        while((line=csvReader.read())!=null){
        }
        Assertions.assertTrue(Objects.isNull(line));
    }

}
