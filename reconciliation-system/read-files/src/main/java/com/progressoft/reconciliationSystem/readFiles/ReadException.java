package com.progressoft.reconciliationSystem.readFiles;

import java.io.IOException;

public class ReadException extends RuntimeException {
    public ReadException(String message) {
        super(message);
    }

    public ReadException(IOException e) {
        super(e);
    }
}
