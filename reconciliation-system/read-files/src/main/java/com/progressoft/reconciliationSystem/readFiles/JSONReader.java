package com.progressoft.reconciliationSystem.readFiles;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Currency;
import java.util.HashMap;
import java.util.regex.Pattern;

class JSONReader extends CommonUse implements FileFormatReader, DateValidator {
    private boolean firstRead = true;
    Path path;

    JSONReader(Path path) {
        PathValidator.validatePath(path);
        this.path = path;
        records = new HashMap<>();
    }

    @Override
    public Record read() {
        try (java.io.FileReader fileReader = new java.io.FileReader(String.valueOf(path))) {
            Object object = new JSONParser().parse(fileReader);
            JSONArray jsonArray = (JSONArray) object;
            if (firstRead) {
                readAllRecordsAsString(jsonArray);
                firstRead = false;
                setCurrentIndexToZero();
            }
        } catch (ParseException e) {
            throw new IllegalStateException("Error in file ",e);
        } catch (IOException e) {
            throw new IllegalStateException("Error while reading the file ",e);
        }
        if (isFileEnd())
            return null;
        Record currentRecord = getCurrentRecord();
        if (amountHaveNegativeValue(currentRecord.getAmount()))
            throw new IllegalStateException("Amount have negative value at index : " + currentIndex);

        return currentRecord;
    }

    @Override
    public boolean isValidDateFormat(String s) {
        return Pattern.matches("^([0]?[1-9]|[1|2][0-9]|[3][0|1])[/-]([0]?[1-9]|[1][0-2])[/-]([0-9]{4})$", s);
    }

    private Record getCurrentRecord() {
        return records.get(currentIndex++);
    }

    private void readAllRecordsAsString(JSONArray jsonArray) {
        jsonArray.forEach(record -> parseRecordToString((JSONObject) record));
    }

    private void parseRecordToString(JSONObject record) {
        try {
            String ID = (String) record.get("reference");
            String currency = (String) record.get("currencyCode");
            String amount = (String) record.get("amount");
            BigDecimal amountValue;
            try {
                amountValue = new BigDecimal(amount).setScale(Currency.getInstance(currency).getDefaultFractionDigits());
            } catch (Exception e) {
                throw new IllegalStateException("Amount have wrong format : ",e);
            }

            String date = (String) record.get("date");
            if (!isValidDateFormat(date))
                throw new IllegalStateException("Wrong date format");
            date = flipDate(date);

            Record currentRecord=new Record(ID,amountValue,currency,date);
            records.put(currentIndex++, currentRecord);
        } catch (Exception e) {
            throw new IllegalStateException("Error at index : " + (currentIndex + 1),e);
        }
    }

    private String flipDate(String date) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(date, 6, 10).append("-");
        stringBuilder.append(date, 3, 5).append("-");
        stringBuilder.append(date, 0, 2);
        return stringBuilder.toString();
    }
}
