package com.progressoft.reconciliationSystem.readFiles;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

class PathValidator {
    public static void validatePath(Path path) {
        if(Objects.isNull(path))
            throw new IllegalStateException("null path");
        if(!Files.exists(path))
            throw new ReadException("file doesn't exist");
    }
}
