package com.progressoft.reconciliationSystem.readFiles;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Currency;
import java.util.HashMap;
import java.util.regex.Pattern;

class CSVReader extends CommonUse implements FileFormatReader, DateValidator {
    private BufferedReader bufferedReader;
    private Path path;

    CSVReader(Path path) {
        PathValidator.validatePath(path);
        this.path = path;
        records = new HashMap<>();
    }

    @Override
    public Record read() {
        if (isFirstRead()) {
            initiateBufferedReader();
            readAllRecords();
            setCurrentIndexToZero();
        }
        if (fileIsEmpty())
            throw new IllegalStateException("Less Data than expected");

        if (isFileEnd())
            return null;

        Record currentRow = getCurrentRow();
        checkRecord(currentRow);
        return currentRow;
    }

    @Override
    public boolean isValidDateFormat(String date) {
        return Pattern.matches("\\d{4}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])*", date);
    }

    private void checkRecord(Record currentRow) {
        if (amountHaveNegativeValue(currentRow.getAmount()))
            throw new IllegalStateException("Amount have negative value at index : " + currentIndex);
        if (!isValidDateFormat(currentRow.getDate()))
            throw new IllegalStateException("Wrong date format at index : " + currentIndex);
    }

    private boolean fileIsEmpty() {
        return records.size() == 0;
    }

    public void checkIfAllRequiredColumnsExist(String[] header) {
        throwIfInconsistencyFile(header);
        boolean[] contains = new boolean[3];
        for (int i = 0; i < header.length; i++) {
            checkIfRequired(header, contains, i);
        }
        if (!containsRequired(contains))
            throw new IllegalStateException("Less data than expected");
    }

    private Record getCurrentRow() {
        return records.get(currentIndex++);
    }

    private void readAllRecords() {
        checkHeader();
        String line;
        while (true) {
            try {
                if ((line = bufferedReader.readLine()) == null) break;
            } catch (IOException e) {
                throw new ReadException(e);
            }
            records.put(currentIndex++, getRequiredColumns(line));
        }
    }

    private Record getRequiredColumns(String line) {
        String[] row = line.split(",");

        throwIfInconsistencyFile(row);

        String ID = row[0];
        String amount = row[2];
        String currency = row[3];
        String date = row[5];
        BigDecimal amountValue;

        try {
            amountValue = new BigDecimal(amount).setScale(Currency.getInstance(currency).getDefaultFractionDigits());
        } catch (Exception e) {
            throw new IllegalStateException("Amount have wrong format at index : " + (currentIndex) + " ",e);
        }

        Record currentRecord = new Record(ID, amountValue, currency, date);

        return currentRecord;
    }

    private void throwIfInconsistencyFile(String[] row) {
        if (row.length != 7)
            throw new IllegalStateException("Inconsistency file");
    }

    private void checkHeader() {
        String firstLine;
        try {
            if ((firstLine = bufferedReader.readLine()) != null)
                checkIfAllRequiredColumnsExist(firstLine.split(","));
            else
                throw new IllegalStateException("Less data than expected");
        } catch (IOException e) {
            throw new ReadException(e);
        }
    }

    private boolean isFirstRead() {
        return bufferedReader == null;
    }


    private boolean containsRequired(boolean[] contains) {
        return contains[0] && contains[1] && contains[2];
    }

    private void checkIfRequired(String[] header, boolean[] contains, int i) {
        if (header[i].equals("amount"))
            contains[0] = true;
        else if (header[i].equals("currecny"))
            contains[1] = true;
        else if (header[i].equals("value date"))
            contains[2] = true;
    }

    private void initiateBufferedReader() {
        try {
            bufferedReader = new BufferedReader(new java.io.FileReader(String.valueOf(path)));
        } catch (FileNotFoundException e) {
            throw new ReadException(e);
        }
    }
}
