package com.progressoft.reconciliationSystem.readFiles;

public interface FileFormatReader {
    Record read();
}
