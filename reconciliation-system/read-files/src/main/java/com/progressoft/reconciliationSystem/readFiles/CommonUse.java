package com.progressoft.reconciliationSystem.readFiles;

import java.math.BigDecimal;
import java.util.HashMap;

abstract class CommonUse {
    protected HashMap<Integer,Record> records;
    protected int currentIndex;

    public void setCurrentIndexToZero() {
        currentIndex = 0;
    }

    public boolean isFileEnd() {
        return currentIndex >= records.size();
    }

    boolean amountHaveNegativeValue(BigDecimal amount){
        return amount.compareTo(BigDecimal.ZERO)<0;
    }
}
