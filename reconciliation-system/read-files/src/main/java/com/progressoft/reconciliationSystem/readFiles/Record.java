package com.progressoft.reconciliationSystem.readFiles;

import java.math.BigDecimal;
import java.util.Objects;

public class Record {
    private String ID;
    private String currency;
    private BigDecimal amount;
    private String date;

    public Record(String ID, BigDecimal amount, String currency, String date) {
        this.ID = ID;
        this.currency = currency;
        this.amount = amount;
        this.date = date;
    }

    public String getID() {
        return ID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    public String toStringRecord() {
        return ID + "," + amount + "," + currency + "," + date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return Objects.equals(ID, record.ID) &&
                Objects.equals(currency, record.currency) &&
                amount.equals(record.amount) &&
                Objects.equals(date, record.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, currency, amount, date);
    }
}
