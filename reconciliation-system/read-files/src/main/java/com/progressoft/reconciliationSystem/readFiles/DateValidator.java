package com.progressoft.reconciliationSystem.readFiles;

public interface DateValidator {
    boolean isValidDateFormat(String row);
}
