package com.progressoft.reconciliationSystem.readFiles;

import java.nio.file.Path;

public class FileReader {
    private FileFormatReader fileFormatReader;

    public FileReader(Path path, String extension) {
        PathValidator.validatePath(path);
        validateExtension(path, extension);
        fileFormatReader = extension.equalsIgnoreCase("csv") ? new CSVReader(path) : new JSONReader(path);
    }

    public Record readNextRow(){
        return fileFormatReader.read();
    }

    private void validateExtension(Path path, String extension) {
        throwIfNullExtension(extension);
        throwIfGivenExtensionMismatchPathExtension(path, extension);
        validateFileExtension(extension);

    }

    private void validateFileExtension(String extension) {
        if (isSupported(extension))
            throw new IllegalArgumentException("file extension doesn't provided");
    }

    private boolean isSupported(String extension) {
        return !extension.equalsIgnoreCase("csv") && !extension.equalsIgnoreCase("json");
    }


    private void throwIfGivenExtensionMismatchPathExtension(Path path, String extension) {
        if (!path.toString().endsWith("." + extension))
            throw new IllegalArgumentException("file extension mismatch the expected extension");
    }

    private void throwIfNullExtension(String s) {
        if (s == null)
            throw new IllegalArgumentException("null extension");
    }

}