package com.progressoft.reconciliationSystem.print;

import java.io.IOException;

public class FileReaderException extends RuntimeException {
    public FileReaderException(String message, IOException e){
        super(message,e);
    }

    public FileReaderException(IOException e) {

    }
}
