package com.progressoft.reconciliationSystem.print;

import com.progressoft.reconciliationSystem.matching.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

public class JSONFileWriter implements FilePrinter {
    @Override
    public void print(Result output, String outputPath) {
        validateArguments(output, outputPath);

        ArrayList<String> match = output.getMatch();
        ArrayList<String> mismatch = output.getMismatch();
        ArrayList<String> missing = output.getMissing();

        printJSONRecords(match, Paths.get(outputPath), "match");
        printJSONRecords(mismatch, Paths.get(outputPath), "mismatch");
        printJSONRecords(missing, Paths.get(outputPath), "missing");
    }

    private void validateArguments(Result output, String outputPath) {
        if (Objects.isNull(output))
            throw new IllegalStateException("output Result is null");
        if ((Objects.isNull(outputPath)) || !Files.isDirectory(Paths.get(outputPath)))
            throw new IllegalArgumentException("output path not directory");
    }

    private void printJSONRecords(ArrayList<String> records, Path path, String fileName) {
        JSONArray jsonArray = new JSONArray();
        for (String record : records) {
            JSONObject object = new JSONObject();
            String[] splittedRecord = record.split(",");
            if (!fileName.equals("match")) {
                fillMismatchOrMissing(object, splittedRecord);
            } else {
                fillMatch(object, splittedRecord);
            }
            jsonArray.add(object);
        }
        try (FileWriter writer = new FileWriter(path + "/" + fileName + ".json")) {
            writer.write(jsonArray.toJSONString());
            writer.flush();
        } catch (IOException e) {
            throw new FileReaderException("No such path exists ", e);
        }
    }

    private void fillMatch(JSONObject object, String[] splittedRecord) {
        object.put("Transaction ID", splittedRecord[0]);
        object.put("Amount", splittedRecord[1]);
        object.put("currency", splittedRecord[2]);
        object.put("Value date", splittedRecord[3]);
    }

    private void fillMismatchOrMissing(JSONObject object, String[] splittedRecord) {
        object.put("Found in File", splittedRecord[0]);
        object.put("Transaction ID", splittedRecord[1]);
        object.put("Amount", splittedRecord[2]);
        object.put("currency", splittedRecord[3]);
        object.put("Value date", splittedRecord[4]);
    }
}
