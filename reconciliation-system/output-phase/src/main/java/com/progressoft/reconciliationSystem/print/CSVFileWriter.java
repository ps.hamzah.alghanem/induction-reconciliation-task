package com.progressoft.reconciliationSystem.print;

import com.progressoft.reconciliationSystem.matching.Result;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

public class CSVFileWriter implements FilePrinter {

    @Override
    public void print(Result output, String outputPath) {
        validateArguments(output, outputPath);
        ArrayList<String> match = output.getMatch();
        ArrayList<String> mismatch = output.getMismatch();
        ArrayList<String> missing = output.getMissing();
        Path path=Paths.get(outputPath);
        printRecords(match,path,"match");
        printRecords(mismatch,path,"mismatch");
        printRecords(missing,path,"missing");
    }

    private void validateArguments(Result output, String outputPath) {
        if (Objects.isNull(output))
            throw new IllegalStateException("output Result is null");
        if ((Objects.isNull(outputPath)) || !Files.isDirectory(Paths.get(outputPath)))
            throw new IllegalArgumentException("output path not directory");
    }

    private void printRecords(ArrayList<String> records, Path outputPath, String fileName) {
        try (PrintWriter printWriter = new PrintWriter(new java.io.FileWriter(outputPath + "/" + fileName + ".txt"))) {
            if (isMatch(fileName)) {
                printHeader(printWriter, "transaction id,amount,currency code,value date");
            } else {
                printHeader(printWriter, "found in file,transaction id,amount,currency code,value date");
            }

            for (String record : records) {
                printWriter.println(record);
            }
        } catch (IOException e) {
            throw new FileReaderException("No such path exist " , e);
        }
    }

    private void printHeader(PrintWriter printWriter, String s) {
        printWriter.println(s);
    }

    private boolean isMatch(String fileName) {
        return fileName.equalsIgnoreCase("match");
    }
}
