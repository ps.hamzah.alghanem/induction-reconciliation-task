package com.progressoft.reconciliationSystem.print;

import com.progressoft.reconciliationSystem.matching.Result;

public interface FilePrinter {
    void print(Result output, String outputPath);
}
