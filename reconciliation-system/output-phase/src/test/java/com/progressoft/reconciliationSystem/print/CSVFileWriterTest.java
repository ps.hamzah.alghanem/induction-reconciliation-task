package com.progressoft.reconciliationSystem.print;

import com.progressoft.reconciliationSystem.matching.Result;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

public class CSVFileWriterTest {
    @Test
    public void givenNullResult_whenPrinting_thenThrowIllegalStateException() {
        Result output = null;
        CSVFileWriter CSVFileWriter = new CSVFileWriter();
        IllegalStateException illegalArgumentException = Assertions.assertThrows(IllegalStateException.class, () -> CSVFileWriter.print(output, "."));
        Assertions.assertEquals("output Result is null", illegalArgumentException.getMessage());
    }

    @Test
    public void givenInvalidPath_whenPrinting_thenThrowIllegalArgumentException() {
        Result output = new Result(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        CSVFileWriter fileWriter = new CSVFileWriter();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> fileWriter.print(output, null));
        Assertions.assertEquals("output path not directory", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> fileWriter.print(output, "/home/user/equation.txt"));
        Assertions.assertEquals("output path not directory", exception.getMessage());
    }

    @Test
    public void givenValidMapAndPath_whenPrinting_thenOutputShouldBeAsExpected() throws FileNotFoundException {
        ArrayList<String> matching = new ArrayList<>();
        ArrayList<String> mismatch = new ArrayList<>();
        ArrayList<String> missing = new ArrayList<>();
        matching.add("TR-47884222201,140.00,USD,2020-01-20");
        Result actual = new Result(matching, mismatch, missing);
        String path;
        try {
            path=String.valueOf(Files.createTempDirectory("temp"));
        } catch (IOException e) {
            throw new FileReaderException("Error in file ",e);
        }
        new CSVFileWriter().print(actual,path);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path+"/match.txt"));
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            Assertions.assertEquals("TR-47884222201,140.00,USD,2020-01-20", line);
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw new FileReaderException("Error with reading the file",e);
        }
    }
}
