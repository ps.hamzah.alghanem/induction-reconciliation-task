package com.progressoft.reconciliationSystem.print;

import com.progressoft.reconciliationSystem.matching.Result;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class JSONFileWriterTest {
    @Test
    public void givenNullResult_whenPrinting_thenThrowIllegalStateException() {
        Result output = null;
        JSONFileWriter CSVFileWriter = new JSONFileWriter();
        IllegalStateException illegalArgumentException = Assertions.assertThrows(IllegalStateException.class, () -> CSVFileWriter.print(output, "."));
        Assertions.assertEquals("output Result is null", illegalArgumentException.getMessage());
    }

    @Test
    public void givenInvalidPath_whenPrinting_thenThrowIllegalArgumentException() {
        Result output = new Result(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        JSONFileWriter fileWriter = new JSONFileWriter();
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> fileWriter.print(output, null));
        Assertions.assertEquals("output path not directory", exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> fileWriter.print(output, "/home/user/equation.txt"));
        Assertions.assertEquals("output path not directory", exception.getMessage());
    }

    @Test
    public void givenValidMapAndPath_whenPrinting_thenOutputShouldBeAsExpected() {
        ArrayList<String> matching = new ArrayList<>();
        ArrayList<String> mismatch = new ArrayList<>();
        ArrayList<String> missing = new ArrayList<>();
        matching.add("TR-47884222201,140.00,USD,2020-01-20");
        matching.add("TR-47884222203,5000.000,JOD,2020-01-25");
        matching.add("TR-47884222206,500.00,USD,2020-02-10");

        Result actual = new Result(matching, mismatch, missing);
        String path;
        try {
            path = String.valueOf(Files.createTempDirectory("temp"));
        } catch (IOException e) {
            throw new FileReaderException("Error in file ", e);
        }
        new JSONFileWriter().print(actual, path);
        try {
            FileUtils.contentEquals(new File(path + "/match.txt"), new File("src/test/resources/match.json"));
        } catch (IOException e) {
            throw new FileReaderException("Error with reading file", e);
        }
    }
}
