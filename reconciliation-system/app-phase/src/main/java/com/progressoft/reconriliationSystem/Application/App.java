package com.progressoft.reconriliationSystem.Application;

import com.progressoft.reconciliationSystem.managment.FileDetails;
import com.progressoft.reconciliationSystem.managment.FileReconciliationGenerator;
import com.progressoft.reconciliationSystem.matching.RecordMatcher;
import com.progressoft.reconciliationSystem.print.CSVFileWriter;

import java.net.URI;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        String sourceLocation, targetLocation, sourceExtension, targetExtension;
        Scanner reader = new Scanner(System.in);
        System.out.println(">> Enter source file location:");
        sourceLocation = reader.nextLine();
        System.out.println(">> Enter source file format:");
        sourceExtension = reader.nextLine();
        System.out.println(">> Enter target file location:");
        targetLocation = reader.nextLine();
        System.out.println(">> Enter target file format:");
        targetExtension = reader.nextLine();

        URI generatedFilesLocation = generate(sourceLocation,targetLocation,sourceExtension,targetExtension);
        System.out.println("Reconciliation finished.\nResult files are available in directory " + generatedFilesLocation);
    }

    public static URI generate(String sourceLocation, String targetLocation, String sourceExtension, String targetExtension) {
        FileDetails source=new FileDetails(sourceLocation,sourceExtension);
        FileDetails target=new FileDetails(targetLocation,targetExtension);
        CSVFileWriter filePrinter = new CSVFileWriter();
        RecordMatcher recordMatcher = new RecordMatcher();
        URI generate = new FileReconciliationGenerator(filePrinter, recordMatcher).generate(source, target).toUri();
        return generate;

    }
}
