package com.progressoft.reconciliationSystem.managment;

import com.progressoft.reconciliationSystem.matching.RecordMatcher;
import com.progressoft.reconciliationSystem.matching.Result;
import com.progressoft.reconciliationSystem.print.CSVFileWriter;
import com.progressoft.reconciliationSystem.print.FilePrinter;
import com.progressoft.reconciliationSystem.print.JSONFileWriter;
import com.progressoft.reconciliationSystem.readFiles.FileReader;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class WebReconciliationGenerator implements ReconciliationGenerator {
    private FilePrinter filePrinter;
    private HttpServletRequest request;
    private RecordMatcher matcher;

    public WebReconciliationGenerator(String extension, HttpServletRequest request, RecordMatcher matcher) {
        this.filePrinter = (extension.equalsIgnoreCase("csv") ? new CSVFileWriter() : new JSONFileWriter());
        this.request = request;
        this.matcher = matcher;
    }

    @Override
    public Path generate(FileDetails source, FileDetails target) {
        validateFiles(source, target);
        try {
            String tempPath = String.valueOf(Files.createTempDirectory("temp"));
            FileReader sourceFile = new FileReader(source.getPath(), source.getExtension());
            FileReader targetFile = new FileReader(target.getPath(), target.getExtension());

            Result result = matcher.prepareFiles(sourceFile, targetFile);
            request.getSession().setAttribute("Result", result);
            filePrinter.print(result, tempPath);
            return Paths.get(tempPath);
        } catch (IOException e) {
            throw new MemoryFileException("Can't use memory", e);
        }

    }

    private void validateFiles(FileDetails source, FileDetails target) {
        if (Objects.isNull(source) || Objects.isNull(target))
            throw new IllegalArgumentException("Null file");
    }
}
