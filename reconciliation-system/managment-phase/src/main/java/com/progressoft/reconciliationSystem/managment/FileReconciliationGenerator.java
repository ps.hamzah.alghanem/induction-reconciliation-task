package com.progressoft.reconciliationSystem.managment;

import com.progressoft.reconciliationSystem.matching.RecordMatcher;
import com.progressoft.reconciliationSystem.matching.Result;
import com.progressoft.reconciliationSystem.print.FilePrinter;
import com.progressoft.reconciliationSystem.readFiles.FileReader;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class FileReconciliationGenerator implements ReconciliationGenerator{
    private FilePrinter filePrinter;
    private RecordMatcher recordMatcher;

    public FileReconciliationGenerator(FilePrinter filePrinter, RecordMatcher recordMatcher){
        this.filePrinter=filePrinter;
        this.recordMatcher=recordMatcher;
    }

    @Override
    public Path generate(FileDetails source, FileDetails target) {
        validateFiles(source, target);
        FileReader sourceFile = new FileReader(source.getPath(), source.getExtension());
        FileReader targetFile = new FileReader(target.getPath(), target.getExtension());

        Result result;
        // TODO this should be a dependency passed to generator 'review'
        result = recordMatcher.prepareFiles(sourceFile, targetFile);
        filePrinter.print(result, ".");

        return Paths.get("");
    }

    private void validateFiles(FileDetails source, FileDetails target) {
        if (Objects.isNull(source) || Objects.isNull(target))
            throw new IllegalArgumentException("Null file");
    }

}
