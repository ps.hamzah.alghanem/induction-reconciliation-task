package com.progressoft.reconciliationSystem.managment;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class FileDetails {
    private Path path;
    private String extension;

    public FileDetails(String path, String extension) {
        validateNullArguments(path, extension);
        validatePathExists(path);
        this.path = Paths.get(path);
        this.extension = extension;
    }

    private void validatePathExists(String path) {
        if(!isValidPath(path))
            throw new IllegalArgumentException("Path doesn't exist");
    }

    private boolean isValidPath(String path) {
        return Files.exists(Paths.get(path));
    }

    private void validateNullArguments(String path, String extension) {
        checkNull(path);
        checkNull(extension);
    }

    private void checkNull(String object) {
        if(Objects.isNull(object))
            throw new IllegalArgumentException("Null Argument");
    }

    public Path getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }


}
