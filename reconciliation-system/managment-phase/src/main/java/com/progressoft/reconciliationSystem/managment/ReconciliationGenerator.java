package com.progressoft.reconciliationSystem.managment;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;

public interface ReconciliationGenerator {
    Path generate(FileDetails source, FileDetails target);
}
