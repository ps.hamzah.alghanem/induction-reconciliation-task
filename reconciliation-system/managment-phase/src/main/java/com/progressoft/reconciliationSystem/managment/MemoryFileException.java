package com.progressoft.reconciliationSystem.managment;

public class MemoryFileException extends RuntimeException {
    public MemoryFileException(String message, Exception e) {
        super(message, e);
    }
}
