package com.progressoft.reconciliationSystem.managment;

import com.progressoft.reconciliationSystem.matching.RecordMatcher;
import com.progressoft.reconciliationSystem.print.CSVFileWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WebReconciliationGeneratorTest {

    @Test
    public void givenInvalidPath_whenGenerating_throwIllegalStateException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new WebReconciliationGenerator("csv",null,new RecordMatcher()).generate(null, null));
        String correctLocation="src/test/resources/bank-transactions.csv";
        Assertions.assertEquals("Null file",exception.getMessage());
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new WebReconciliationGenerator("csv",null,new RecordMatcher()).generate(new FileDetails(correctLocation,"csv"),null));
        Assertions.assertEquals("Null file",exception.getMessage());

    }
}
