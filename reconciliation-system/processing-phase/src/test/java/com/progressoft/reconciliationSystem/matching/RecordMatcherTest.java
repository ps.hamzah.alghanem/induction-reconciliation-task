package com.progressoft.reconciliationSystem.matching;


import com.progressoft.reconciliationSystem.readFiles.FileReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

public class RecordMatcherTest {

    @Test
    public void givenSourceFileWithDuplicateIDs_whenPreparingFiles_thenThrowIllegalStateException() {
        FileReader source = new FileReader(Paths.get("src/test/resources/bank-transactions2.csv"), "csv");
        FileReader target = new FileReader(Paths.get("src/test/resources/bank-transactions2.csv"), "csv");
        RecordMatcher match = new RecordMatcher();
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class,
                () -> match.prepareFiles(source, target));
        Assertions.assertEquals("File has duplicate transaction ID : TR-47884222201", illegalStateException.getMessage());

    }

    @Test
    public void givenTargetFileWithDuplicateRecords_whenPreparingFiles_thenThrowIllegalStateException() {
        FileReader source = new FileReader(Paths.get("src/test/resources/bank-transactions.csv"), "csv");
        FileReader target = new FileReader(Paths.get("src/test/resources/bank-transactions2.csv"), "csv");
        RecordMatcher match = new RecordMatcher();
        IllegalStateException illegalStateException = Assertions.assertThrows(IllegalStateException.class,
                () -> match.prepareFiles(source, target));
        Assertions.assertEquals("File has duplicate transaction ID : TR-47884222201", illegalStateException.getMessage());
    }

    @Test
    public void givenValidFiles_whenPreparingFiles_thenReturnCorrectOutput() {
        RecordMatcher recordMatcher = new RecordMatcher();

        FileReader source = new FileReader(Paths.get("src/test/resources/bank-transactions.csv"), "csv");
        FileReader target = new FileReader(Paths.get("src/test/resources/online-banking-transactions.json"), "json");

        HashMap<String, ArrayList<String>> expected = new HashMap<>();
        ArrayList<String> match = new ArrayList<>();

        match.add("TR-47884222201,140.00,USD,2020-01-20");
        match.add("TR-47884222203,5000.000,JOD,2020-01-25");
        match.add("TR-47884222206,500.00,USD,2020-02-10");
        expected.put("match", match);

        ArrayList<String> mismatch = new ArrayList<>();
        mismatch.add("SOURCE,TR-47884222202,20.000,JOD,2020-01-22");
        mismatch.add("TARGET,TR-47884222202,30.000,JOD,2020-01-22");
        mismatch.add("SOURCE,TR-47884222205,60.000,JOD,2020-02-02");
        mismatch.add("TARGET,TR-47884222205,60.000,JOD,2020-02-03");
        expected.put("mismatch", mismatch);

        ArrayList<String> missing = new ArrayList<>();
        missing.add("SOURCE,TR-47884222204,1200.000,JOD,2020-01-31");
        missing.add("TARGET,TR-47884222217,12000.000,JOD,2020-02-14");
        missing.add("TARGET,TR-47884222245,420.00,USD,2020-01-12");
        expected.put("missing", missing);

        Result result = recordMatcher.prepareFiles(source, target);
        Assertions.assertLinesMatch(match,result.getMatch());
        Assertions.assertLinesMatch(mismatch,result.getMismatch());
        Assertions.assertLinesMatch(missing,result.getMissing());
    }
}