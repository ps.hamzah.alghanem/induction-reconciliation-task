package com.progressoft.reconciliationSystem.matching;

import java.util.ArrayList;

public class Result {
    private ArrayList<String> match;
    private ArrayList<String> mismatch;
    private ArrayList<String> missing;

    public Result(ArrayList<String> match,ArrayList<String> mismatch,ArrayList<String> missing){
        this.match=match;
        this.mismatch=mismatch;
        this.missing=missing;
    }

    public ArrayList<String> getMatch() {
        return match;
    }

    public ArrayList<String> getMismatch() {
        return mismatch;
    }

    public ArrayList<String> getMissing() {
        return missing;
    }
}
