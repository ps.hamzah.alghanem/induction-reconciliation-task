package com.progressoft.reconciliationSystem.matching;


import com.progressoft.reconciliationSystem.readFiles.FileReader;
import com.progressoft.reconciliationSystem.readFiles.Record;

import java.util.*;

public class RecordMatcher {
    // TODO those belongs to a result class 'review'
    private TreeMap<String, Record> sourceRecords;
    private TreeMap<String, Record> targetRecords;
    private ArrayList<String> matchRecords;
    private ArrayList<String> mismatchRecords;
    private ArrayList<String> missingRecords;

    public RecordMatcher() {
        sourceRecords = new TreeMap<>();
        targetRecords = new TreeMap<>();
        matchRecords = new ArrayList<>();
        mismatchRecords = new ArrayList<>();
        missingRecords = new ArrayList<>();
    }

    public Result prepareFiles(FileReader source, FileReader target) {
        validateReaders(source, target);

        fillLists(source, target);

        matchRecords();

        Result result = new Result(matchRecords, mismatchRecords, missingRecords);

        return result;
    }

    private void matchRecords() {

        Set<String> sourceIDs = sourceRecords.keySet();
        Set<String> targetIDs = targetRecords.keySet();
        for (String ID : sourceIDs) {
            if (!targetContainsID(targetIDs, ID)) {
                addToMissingList(sourceRecords.get(ID), "SOURCE,");
                continue;
            }
            if (isEqualRecords(ID))
                addToMatchList(sourceRecords.get(ID));
            else
                addToMismatchList(sourceRecords.get(ID), targetRecords.get(ID));
            deleteFromTargetRecords(ID);
        }

        for (String ID : targetIDs) {
            addToMissingList(targetRecords.get(ID), "TARGET,");
        }
    }

    private boolean isEqualRecords(String ID) {
        return sourceRecords.get(ID).equals(targetRecords.get(ID));
    }

    private boolean targetContainsID(Set<String> targetIDs, String ID) {
        return targetIDs.contains(ID);
    }

    private void addToMissingList(Record sourceRecord, String foundInFile) {
        missingRecords.add(foundInFile + sourceRecord.toStringRecord());
    }

    private void deleteFromTargetRecords(String ID) {
        targetRecords.remove(ID);
    }

    private void addToMismatchList(Record sourceRecord, Record targetRecord) {
        mismatchRecords.add("SOURCE," + sourceRecord.toStringRecord());
        mismatchRecords.add("TARGET," + targetRecord.toStringRecord());
    }

    private void addToMatchList(Record sourceRecord) {
        matchRecords.add(sourceRecord.toStringRecord());
    }

    private void fillLists(FileReader source, FileReader target) {
        insertRecords(source, sourceRecords);
        insertRecords(target, targetRecords);
    }

    private void insertRecords(FileReader file, TreeMap<String, Record> records) {
        Record line;
        while ((line = file.readNextRow()) != null) {
            throwIfDuplicateID(records, line);
            records.put(line.getID(),line);
        }
    }

    private void throwIfDuplicateID(TreeMap<String, Record> records, Record line) {
        if (records.containsKey(line.getID()))
            throw new IllegalStateException("File has duplicate transaction ID : " + line.getID());
    }

    private void validateReaders(FileReader source, FileReader target) {
        if (Objects.isNull(source))
            throw new IllegalArgumentException("source file is null");
        if (Objects.isNull(target))
            throw new IllegalArgumentException("target file is null");
    }

}