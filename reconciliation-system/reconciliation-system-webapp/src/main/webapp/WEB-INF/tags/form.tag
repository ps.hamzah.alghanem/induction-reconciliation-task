<%@attribute name="submitUrl" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="formMethod" rtexprvalue="true" required="false" type="java.lang.String" %>
<%@attribute name="fileName" required="true" rtexprvalue="true" type="java.lang.String" %>

<%@taglib prefix="con" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${formMethod eq null}">
    <c:set var="formMethod" value="POST"/>
</c:if>

<form action="${submitUrl}"  method="${formMethod}" enctype="multipart/form-data">
    <div class="fadeIn first">
        <img src="${pageContext.request.contextPath}/bootstrap/images/progressoft.png" id="icon" alt="User Icon"/>
    </div>
    <div class="fadeIn second">
        <p>${fileName} Name:</p><br/>
    </div>
    <input type="text" id="${fileName}" class="fadeIn second" name="${fileName}" placeholder="File Name" required/>
    <label class="fadeIn second" for="types">
        <p>File type:</p>
    </label>
    <select name="types" id="types">
        <option value="csv">csv</option>
        <option value="json">json</option>
    </select>

    <div class="fadeIn second">
        <p>File:</p><br/>
    </div>
    <input type="file" name="${fileName}" required/>

    <input type="submit" class="fadeIn fourth"/>&nbsp;
    <input type="reset" class="fadeIn fourth"/>
</form>
