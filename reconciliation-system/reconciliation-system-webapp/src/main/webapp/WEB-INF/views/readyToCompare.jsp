Ready To Compare
</title>
</head>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <div class="fadeIn first">
            <img src="${pageContext.request.contextPath}/bootstrap/images/progressoft.png" id="icon" alt="User Icon"/>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body fadeIn second">
                        <h5 class="card-title">Source</h5>
                        <p class="card-text">Name: ${sessionScope.get("Source")}</p>
                        <p class="card-text">Type: ${sessionScope.get("SourceType")}</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body fadeIn second">
                        <h5 class="card-title">Target</h5>
                        <p class="card-text">Name: ${sessionScope.get("Target")}</p>
                        <p class="card-text">Type: ${sessionScope.get("TargetType")}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="fadeIn second">
            <p class="card-text">
                Result Files Format:
            </p>
            <select class="form-control" id="FileFormat">
                <option value="csv">csv</option>
                <option value="json">json</option>
            </select>
        </div>
        <div class="fadeIn third">
            <script>
                function sendPost() {
                    let fileFormat=document.getElementById("FileFormat").value
                    var jqXHR = $.ajax({
                        type: 'post'
                        , url: "${pageContext.request.contextPath}/readyToCompare"
                        , headers: {

                        }
                        , data: {
                            'FileFormat': fileFormat

                        },
                        success: function (data) {
                            location.replace("${pageContext.request.contextPath}/result")
                        }
                    })
                }
            </script>
            <a href="${pageContext.request.contextPath}/sourceUpload" class="fadeIn fourth"><input type="button" value="Cancel"/></a>
            <a href="${pageContext.request.contextPath}/targetUpload" class="fadeIn fourth"><input type="button" value="Back"/></a>
            <input type="button" value="Compare"onclick="sendPost()" name="compare" id="compare" class="fadeIn fourth"/>
        </div>
    </div>

</div>
