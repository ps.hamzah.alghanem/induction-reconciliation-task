<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
Result
</title>
</head>

<body>
<script>
    $(document).ready(function () {
        $("#Mismatch").removeClass("active");
        $("#Missing").removeClass("active");
        $("#MismatchBody").hide();
        $("#MissingBody").hide();

        $("#Match").addClass("active");
        $("#MatchBody").show();

        $("#Match").on("click", function () {
            $("#Mismatch").removeClass("active");
            $("#Missing").removeClass("active");
            $("#MismatchBody").hide();
            $("#MissingBody").hide();

            $("#Match").addClass("active");
            $("#MatchBody").show();
        });

        $("#Mismatch").on("click", function () {
            $("#Match").removeClass("active");
            $("#Missing").removeClass("active");
            $("#MatchBody").hide();
            $("#MissingBody").hide();

            $("#Mismatch").addClass("active");
            $("#MismatchBody").show();
        });

        $("#Missing").on("click", function () {
            $("#Match").removeClass("active");
            $("#Mismatch").removeClass("active");
            $("#MismatchBody").hide();
            $("#MatchBody").hide();

            $("#Missing").addClass("active");
            $("#MissingBody").show();
        })


    });
</script>

<ul class="nav nav-tabs">
    <li class="nav-item">
        <label class="nav-link active" id="Match">Match</label>
        <div id="MatchBody">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Transaction ID</th>
                    <th scope="col">Amount</th>
                    <th scope="col">currency</th>
                    <th scope="col">Value Date</th>
                </tr>
                </thead>
                <tbody>


            <c:forEach items="${sessionScope.get('Result').match}" var="value" varStatus="loop">
                <tr><td><c:out value="${loop.index+1}"/></td>
                    <c:forEach items="${value}" var="record">
                    <td><c:out value="${record}"/></td>
                </c:forEach></tr>
            </c:forEach>
            </tbody>
            </table>
        </div>
    </li>
    <li class="nav-item">
        <label class="nav-link" id="Mismatch">Mismatch</label>
        <div id="MismatchBody">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Found In File</th>
                    <th scope="col">Transaction ID</th>
                    <th scope="col">Amount</th>
                    <th scope="col">currency</th>
                    <th scope="col">Value Date</th>
                </tr>
                </thead>
                <tbody>
            <c:forEach items="${sessionScope.get('Result').mismatch}" var="value" varStatus="loop">
                <tr><td><c:out value="${loop.index+1}"/></td>
                    <c:forEach items="${value}" var="record">
                        <td><c:out value="${record}"/></td>
                    </c:forEach></tr>
            </c:forEach>
                </tbody>
            </table>
        </div>
    </li>
    <li class="nav-item">
        <label class="nav-link" id="Missing">Missing</label>
        <div id="MissingBody">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Found In File</th>
                    <th scope="col">Transaction ID</th>
                    <th scope="col">Amount</th>
                    <th scope="col">currency</th>
                    <th scope="col">Value Date</th>
                </tr>
                </thead>
                <tbody>
            <c:forEach items="${sessionScope.get('Result').missing}" var="value" varStatus="loop">
                <tr><td><c:out value="${loop.index+1}"/></td>
                    <c:forEach items="${value}" var="record">
                        <td><c:out value="${record}"/></td>
                    </c:forEach></tr>
            </c:forEach>
                </tbody>
            </table>
        </div>
    </li>
</ul>
<a href="${pageContext.request.contextPath}/sourceUpload"><input type="button" value="Compare new files"></a>
<form method="post" action="${pageContext.request.contextPath}/result"><input type="submit" value="Download"></form>
</body>