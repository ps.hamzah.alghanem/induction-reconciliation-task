Login
</title>
<body>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first">
            <img src="${pageContext.request.contextPath}/bootstrap/images/progressoft.png" id="icon" alt="User Icon"/>
        </div>

        <!-- Login Form -->

        <form action="${pageContext.request.contextPath}/login" method="post">
            <input type="text" id="username" class="fadeIn second" name="username" placeholder="Username" required/>

                <input type="password" id="password" class="fadeIn third" name="password" placeholder="Password" required/>

            <input type="submit" id="login" class="fadeIn fourth" value="Log In">

        </form>

    </div>
</div>
</body>
</html>